#!/usr/bin/env python3

from lib import get_int_data, Intcode, test

ic = get_int_data("02.in")


def fix(noun, verb, memory, v=False):
    new_mem = memory.copy()
    new_mem[1] = noun
    new_mem[2] = verb
    icp = Intcode(new_mem, return_memory=True)
    return icp.compute()[0]


@test
def tests():
    tests = [
        [1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50],
        [3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50],
        [1, 0, 0, 0, 99],
        [2, 0, 0, 0, 99],
        [2, 3, 0, 3, 99],
        [2, 3, 0, 6, 99],
        [2, 4, 4, 5, 99, 0],
        [2, 4, 4, 5, 99, 9801],
        [1, 1, 1, 4, 99, 5, 6, 0, 99],
        [30, 1, 1, 4, 2, 5, 6, 0, 99],
    ]
    for i in range(0, len(tests), 2):
        icp = Intcode(tests[i], return_memory=True)
        print(icp.compute() == tests[i + 1])


if __name__ == "__main__":
    tests()
    print(fix(12, 2, ic))
