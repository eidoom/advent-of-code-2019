#!/usr/bin/env python3

from pathlib import Path
from pickle import dump, load
from collections import defaultdict, deque

from lib import Intcode, Template, test, timing


class Graph(Template):
    def __init__(self, v=False):
        super().__init__(v)
        self.graph = defaultdict(list)

    def add_edge(self, u, v):
        self.graph[u].append(v)
        self.graph[v].append(u)

    @timing
    def bfs_depth(self, strt):
        visited = defaultdict(lambda: False)
        visited[strt] = True
        queue = deque([strt])
        depth = deque([0])
        traversal = []
        while queue:
            parent = queue.popleft()
            d = depth.popleft()
            traversal.append(parent)
            for child in self.graph[parent]:
                if not visited[child]:
                    queue.append(child)
                    visited[child] = True
                    depth.append(d + 1)
        return d


class RepairDroid(Intcode):
    def __init__(self, v=False, p=False):
        super().__init__("15.in", feedback_mode=True, v=v)
        self.p = p
        self.pos = (0, 0)
        self.wall = "#"
        self.unknown = " "
        self.count = 0
        self.empty = "."
        self.oxygen = "O"
        self.map = {self.pos: self.empty}
        self.vals = {self.pos: self.count}
        self.g = Graph()

    @staticmethod
    def inverse(dct):
        inv = defaultdict(list)
        for k, v in dct.items():
            inv[v].append(k)
        return inv

    @staticmethod
    def compass(mvmt_cmd):
        return {1: (-1, 0), 2: (1, 0), 3: (0, 1), 4: (0, -1)}[mvmt_cmd]

    def get_move(self, mvmt_cmd):
        return tuple(a + b for a, b in zip(self.pos, self.compass(mvmt_cmd)))

    def send_move(self, mvmt_cmd):
        return self.compute([mvmt_cmd])

    def xplr(self):
        while True:
            free = True
            while free:
                if self.p:
                    print(self.rndr_mp())
                    input()
                for mvmt_cmd in range(1, 5):
                    pot_mv = self.get_move(mvmt_cmd)
                    if pot_mv not in self.vals.keys():
                        status = self.send_move(mvmt_cmd)
                        if status == 0:
                            self.map[pot_mv] = self.wall
                            self.vals[pot_mv] = float("inf")
                            if mvmt_cmd == 4:
                                free = False
                        else:
                            self.count += 1
                            self.vals[pot_mv] = self.count
                            self.pos = pot_mv
                            if status == 1:
                                self.map[pot_mv] = self.empty
                                break
                            elif status == 2:
                                self.map[pot_mv] = self.oxygen
                    else:
                        if mvmt_cmd == 4:
                            free = False
            while not free:
                try:
                    best = float("inf")
                    for mvmt_cmd in range(1, 5):
                        pot_mv = self.get_move(mvmt_cmd)
                        if pot_mv == (0, 0):
                            return self.map
                        val = self.vals[pot_mv]
                        if val < best:
                            best = val
                            nxt = mvmt_cmd
                    self.send_move(nxt)
                    self.pos = self.get_move(nxt)
                    self.count -= 1
                except KeyError:
                    free = True

    def rndr_mp(self):
        coords = [k for k, v in self.map.items()]
        ii, jj = zip(*coords)
        mp = ""
        for i in range(min(ii), max(ii) + 1):
            for j in range(min(jj), max(jj) + 1):
                try:
                    sym = self.map[(i, j)]
                except KeyError:
                    sym = self.unknown
                mp += sym
            mp += "\n"
        return mp

    @timing
    def build_graph(self):
        strt = self.inverse(self.map)[self.oxygen][0]
        self.pos = strt
        self.count = 0
        self.ovals = {self.pos: self.count}
        end = 0
        while True:
            free = True
            while free:
                for mvmt_cmd in range(1, 5):
                    pot_mv = self.get_move(mvmt_cmd)
                    if pot_mv not in self.ovals.keys():
                        if self.map[pot_mv] is self.wall:
                            self.ovals[pot_mv] = float("inf")
                            if mvmt_cmd == 4:
                                free = False
                        else:
                            self.count += 1
                            self.ovals[pot_mv] = self.count
                            self.g.add_edge(self.pos, pot_mv)
                            self.pos = pot_mv
                            break
                    else:
                        if mvmt_cmd == 4:
                            free = False
            while not free:
                try:
                    best = float("inf")
                    for mvmt_cmd in range(1, 5):
                        pot_mv = self.get_move(mvmt_cmd)
                        if pot_mv == strt:
                            end += 1
                            if end > 4:
                                return self.g.graph
                        val = self.ovals[pot_mv]
                        if val < best:
                            best = val
                            nxt = mvmt_cmd
                    self.send_move(nxt)
                    self.pos = self.get_move(nxt)
                    self.count -= 1
                except KeyError:
                    free = True


@test
def tst():
    rd = RepairDroid()
    with open("15t.in", "r") as f:
        mp = f.read().splitlines()
    for i in range(len(mp)):
        for j in range(len(mp[0])):
            rd.map[(i, j)] = mp[i][j]
    print(rd.rndr_mp())
    rd.build_graph()
    strt = rd.inverse(rd.map)[rd.oxygen][0]
    print(rd.g.bfs_depth(strt) == 4)


def memoised():
    rd = RepairDroid()
    memo = "15b.pickle"
    if not Path(memo).is_file():
        with open(memo, "wb") as f:
            dump(rd.xplr(), f)
    else:
        with open(memo, "rb") as f:
            rd.map = load(f)
    print(rd.rndr_mp())
    rd.build_graph()
    strt = rd.inverse(rd.map)[rd.oxygen][0]
    print(rd.g.bfs_depth(strt))


if __name__ == "__main__":
    tst()
    memoised()
