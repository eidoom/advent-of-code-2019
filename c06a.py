#!/usr/bin/env python3

from c01a import get_data


def process_map(filename):
    return [a.split(")") for a in get_data(filename)]


"""
def count_orbits_iterative(filename, v=False):
    if v:
        print("Orbit map: " + " ".join(get_data(filename)))
    test_data = process_map(filename)
    if v:
        print(f"Processed: {test_data}")
    # right orbits left
    orbits = 0
    for i, (left, right) in enumerate(test_data):
        orbits += 1  # direct orbit
        if v:
            print(f"Adding direct orbit {left}){right}")
        while True:
            try:
                i = [a[1] for a in test_data[:i]].index(left)
                left = test_data[i][0]
                orbits += 1  # indirect orbit
                if v:
                    print(f"Adding indirect orbit {left}){right}")
            except ValueError:
                break
    return orbits
"""


def indirect_orbits(i, left, test_data, count=0):
    try:
        new_i = [a[1] for a in test_data].index(left)
        new_left = test_data[new_i][0]
        return indirect_orbits(new_i, new_left, test_data, count + 1)
    except ValueError:
        return count


def count_orbits(filename, v=False):
    test_data = process_map(filename)
    # right orbits left
    return sum(
        [
            1 + indirect_orbits(i, left, test_data)
            for i, (left, right) in enumerate(test_data)
        ]
    )


def test(v=False):
    return count_orbits("06t.in", v) == 42


if __name__ == "__main__":
    print(test())
    print(count_orbits("06.in"))
