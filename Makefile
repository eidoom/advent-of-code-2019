SRCS = $(wildcard *.cpp)
PRGS = $(basename $(SRCS))

CXX = g++
CXXFLAGS = -std=c++17 -O2

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -o $@ -c $<

c12b: %: %.o
	$(CXX) $(CXXFLAGS) -o $@ $^

clean:
	rm -f $(PRGS) *.o