#!/usr/bin/env python3

from collections import defaultdict, deque

from lib import Intcode, Template, timing


class Graph(Template):
    def __init__(self, v=False):
        super().__init__(v)
        self.graph = defaultdict(list)

    def add_edge(self, u, v):
        self.graph[u].append(v)
        self.graph[v].append(u)

    @timing
    def bfs_depth(self, strt):
        visited = defaultdict(lambda: False)
        visited[strt] = True
        queue = deque([strt])
        depth = deque([0])
        traversal = []
        while queue:
            parent = queue.popleft()
            d = depth.popleft()
            traversal.append(parent)
            for child in self.graph[parent]:
                if not visited[child]:
                    queue.append(child)
                    visited[child] = True
                    depth.append(d + 1)
        return d


class RepairDroid(Intcode):
    def __init__(self, v=False, p=False):
        super().__init__("15.in", feedback_mode=True, v=v)
        self.p = p
        self.strt = (0, 0)
        self.pos = self.strt
        self.count = 0
        self.g = Graph()
        self.vals = {self.pos: self.count}
        self.oxygen_supply = None
        if self.p:
            self.wall = "#"
            self.unknown = " "
            self.empty = "."
            self.oxygen = "O"
            self.map = {self.pos: self.empty}
        self.build_graph()

    @staticmethod
    def inverse(dct):
        inv = defaultdict(list)
        for k, v in dct.items():
            inv[v].append(k)
        return inv

    @staticmethod
    def compass(mvmt_cmd):
        return {1: (-1, 0), 2: (1, 0), 3: (0, 1), 4: (0, -1)}[mvmt_cmd]

    def get_move(self, mvmt_cmd):
        return tuple(a + b for a, b in zip(self.pos, self.compass(mvmt_cmd)))

    def send_move(self, mvmt_cmd):
        return self.compute([mvmt_cmd])

    def rndr_mp(self):
        coords = [k for k, v in self.map.items()]
        ii, jj = zip(*coords)
        mp = ""
        for i in range(min(ii), max(ii) + 1):
            for j in range(min(jj), max(jj) + 1):
                try:
                    sym = self.map[(i, j)]
                except KeyError:
                    sym = self.unknown
                mp += sym
            mp += "\n"
        return mp

    @timing
    def build_graph(self):
        end = 0
        while True:
            free = True
            while free:
                if self.p:
                    print(self.rndr_mp())
                for mvmt_cmd in range(1, 5):
                    pot_mv = self.get_move(mvmt_cmd)
                    if pot_mv not in self.vals.keys():
                        status = self.send_move(mvmt_cmd)
                        if status == 0:
                            if self.p:
                                self.map[pot_mv] = self.wall
                            self.vals[pot_mv] = float("inf")
                            if mvmt_cmd == 4:
                                free = False
                        else:
                            self.count += 1
                            self.vals[pot_mv] = self.count
                            self.g.add_edge(self.pos, pot_mv)
                            if self.p:
                                if status == 1:
                                    self.map[pot_mv] = self.empty
                            if status == 2:
                                self.oxygen_supply = pot_mv
                                if self.p:
                                    self.map[pot_mv] = self.oxygen
                            self.pos = pot_mv
                            break
                    else:
                        if mvmt_cmd == 4:
                            free = False
            while not free:
                try:
                    best = float("inf")
                    for mvmt_cmd in range(1, 5):
                        pot_mv = self.get_move(mvmt_cmd)
                        if pot_mv == self.strt:
                            end += 1
                            if end > 4:
                                return self.g.graph
                        val = self.vals[pot_mv]
                        if val < best:
                            best = val
                            nxt_cmd = mvmt_cmd
                            nxt_mv = pot_mv
                    self.send_move(nxt_cmd)
                    self.pos = nxt_mv
                    self.count -= 1
                except KeyError:
                    free = True

    def depth_count(self):
        return self.g.bfs_depth(self.oxygen_supply)


def run():
    rd = RepairDroid(p=True)
    print(rd.depth_count())


if __name__ == "__main__":
    run()
