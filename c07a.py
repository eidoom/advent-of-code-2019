#!/usr/bin/env python3

from itertools import permutations

from lib import Intcode, test

t1pss = [4, 3, 2, 1, 0]  # test 1 phase setting sequence
t1mts = 43210  # test 1 max thruster signal
t2pss = [0, 1, 2, 3, 4]
t2mts = 54321
t3pss = [1, 0, 4, 3, 2]
t3mts = 65210

inpt = 0


# argument: Intcode program, phase setting sequence, input
def run_series(icp, pss, inpt_):
    tmp = inpt_
    # loop over phase settings (for each amplifier)
    for ps in pss:
        amp = Intcode(icp)
        tmp = amp.compute([ps, tmp])[-1]
    return tmp


def maximise_output(icp, inpt_, func, v=False):
    mts = 0
    for pss in permutations(range(5)):
        ts = func(icp, pss, inpt_)
        if v:
            print(ts)
        if ts > mts:
            mts = ts
    return mts


@test
def tests():
    for icp, pss, mts in zip(
        ("07t1.in", "07t2.in", "07t3.in"), (t1pss, t2pss, t3pss), (t1mts, t2mts, t3mts)
    ):
        print(run_series(icp, pss, inpt) == mts)
        print(maximise_output(icp, inpt, run_series) == mts)


if __name__ == "__main__":
    tests()
    print(maximise_output("07.in", inpt, run_series))
