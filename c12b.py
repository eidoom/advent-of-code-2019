#!/usr/bin/env python3

from lib import test
from c12a import Jupiter


class Repeat(Jupiter):
    def __init__(self, filename, v=False):
        super().__init__(filename, v)
        self.memory = [self.copy_p_v()]

    @staticmethod
    def copy_point(point):
        return tuple(a.copy() for a in point)

    def copy_p_v(self):
        return self.copy_point(self.pos), self.copy_point(self.vel)

    def find_repeat(self):
        while True:
            self.do_time_step()
            new = self.copy_p_v()
            if new in self.memory:
                return self.steps
            self.memory += [new]


@test
def test():
    print(Repeat("12t1.in").find_repeat() == 2772)
    print(Repeat("12t2.in").find_repeat() == 4686774924)  # This takes too long...


if __name__ == "__main__":
    test()
