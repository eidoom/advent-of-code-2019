#!/usr/bin/env python3

from lib import Template, get_data


# monitoring station position
# find asteroid that has most other asteroids in view
class MonStaPos(Template):
    def __init__(self, filename, v=False):
        super().__init__(v)
        self.asteroid = "#"
        self.empty = "."
        self.mp = self.import_map(filename)
        self.width = len(self.mp[0])
        self.height = len(self.mp)
        self.pos = (0, 0)

    @staticmethod
    def import_map(filename):
        return [list(s) for s in get_data(filename)]

    def get_map_pt(self, i, j):
        return self.mp[i][j]

    def set_pos(self, i, j):
        self.pos = (i, j)

    def is_asteroid(self, pos=None):
        return self.get_map_pt(*self.pos if pos is None else pos) == self.asteroid

    def is_inside(self, i, j):
        return 0 <= i < self.height and 0 <= j < self.width

    # return position (None) if an asteroid is (not) visible
    # arguments: position, direction
    def see_asteroid(self, drn, pos=None):
        new_pos = tuple(a + b for a, b in zip(self.pos if pos is None else pos, drn))
        if self.is_inside(*new_pos):
            return (
                new_pos
                if self.is_asteroid(new_pos)
                else self.see_asteroid(drn, new_pos)
            )

    # from my project euler repo
    @staticmethod
    def find_greatest_common_divisor(a_, b_):
        a = abs(a_)
        b = abs(b_)
        if b > a:
            a, b = b, a
        gcd = 1
        for i in range(1, b + 1):
            if not a % i and not b % i:
                gcd = i
        return gcd

    def normalise_dir(self, i, j):
        if not i:
            return (0, j // abs(j))
        elif not j:
            return (i // abs(i), 0)
        else:
            return tuple(a // self.find_greatest_common_divisor(i, j) for a in (i, j))

    # count visible asteroids on horizontal line
    # arguments: direction, scanned, count
    def scan_hor(self, drn, scd, cnt=0):
        i, j = drn
        if abs(j) < self.width:
            # don't use null direction
            if i or j:
                dir_n = self.normalise_dir(i, j)
                if dir_n not in scd:
                    scd += [dir_n]
                    if self.see_asteroid(dir_n):
                        cnt += 1
            return self.scan_hor((i, j + 1), scd, cnt)
        return cnt, scd

    # count all visible asteroids
    def scan(self, drn=None, scd=None, cnt=0):
        if scd is None:
            scd = []
        i, j = (-self.height + 1, -self.width + 1) if drn is None else drn
        if abs(i) < self.height:
            new_cnt, scd = self.scan_hor((i, j), scd)
            return self.scan((i + 1, j), scd, cnt + new_cnt)
        return cnt

    # find asteroid position that can see maximum number of other asteroids
    def find_best(self):
        visible = 0
        best = None
        for i in range(self.height):
            for j in range(self.width):
                self.set_pos(i, j)
                if self.is_asteroid():
                    new = self.scan()
                    self.piv(f"Saw {new} asteroids from {self.pos}")
                    if new > visible:
                        visible = new
                        best = self.pos
        return visible, best


# There are 5 tests
# last test is slow, so choose n=4 for quick tests
def tests(n=5):
    files = tuple(f"10t{i}.in" for i in range(1, n + 1))
    answers = ((8, (4, 3)), (33, (8, 5)), (35, (2, 1)), (41, (3, 6)), (210, (13, 11)))
    for f, a in zip(files, answers):
        t = MonStaPos(f)
        print(t.find_best() == a)


# Note that I use (i, j) coordinates
# not (x, y) as in question
if __name__ == "__main__":
    tests(4)
    msp = MonStaPos("10.in")
    num, pos = msp.find_best()
    print(pos)
    print(num)
