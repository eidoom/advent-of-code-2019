#!/usr/bin/env python3

from itertools import combinations

from lib import get_data, Template, test


class Jupiter(Template):
    def __init__(self, filename, v=False):
        super().__init__(v)
        self.pos = tuple(
            [int("".join(filter(lambda y: y.isdigit() or y == "-", x))) for x in moon]
            for moon in get_data(filename)
        )
        self.vel = tuple([0] * 3 for _ in range(4))
        self.steps = 0
        self.piv(self.show())

    def show(self):
        # calculate position and velocity value widths
        # so terminal output is aligned
        # minimum width is two characters
        pw = [2] * 3
        vw = [2] * 3
        for ax in range(3):
            for pos, vel in zip(self.pos, self.vel):
                pw_ = len(str(pos[ax]))
                if pw_ > pw[ax]:
                    pw[ax] = pw_
                vw_ = len(str(vel[ax]))
                if vw_ > vw[ax]:
                    vw[ax] = vw_

        # prepend spaces to values to align columns by width calculated above
        def frmt(inpt, ax, w):
            inpt_str = str(inpt)
            return (" " * (w[ax] - len(inpt_str))) + inpt_str

        # generate terminal output as string
        outpt = f"After {self.steps} step{'' if self.steps == 1 else 's'}:\n"
        for pos, vel in zip(self.pos, self.vel):
            x, y, z = [frmt(a, i, pw) for i, a in enumerate(pos)]
            vx, vy, vz = [frmt(a, i, vw) for i, a in enumerate(vel)]
            outpt += f"pos=<x={x}, y={y}, z={z}>, vel=<x={vx}, y={vy}, z={vz}>\n"
        return outpt + "\n"

    def do_time_step(self):
        for a, b in tuple(combinations(range(4), 2)):
            for ax in range(3):
                first = self.pos[a][ax]
                second = self.pos[b][ax]
                if first > second:
                    self.vel[a][ax] += -1
                    self.vel[b][ax] += 1
                elif first < second:
                    self.vel[a][ax] += 1
                    self.vel[b][ax] += -1
        for pos, vel in zip(self.pos, self.vel):
            for ax in range(3):
                pos[ax] = pos[ax] + vel[ax]
        self.steps += 1
        self.piv(self.show())

    def get_energy(self):
        def en(point):
            return sum([abs(x) for x in point])

        return sum([en(pos) * en(vel) for pos, vel in zip(self.pos, self.vel)])

    def run_sim(self, n):
        while self.steps < n:
            self.do_time_step()
            self.piv(self.show())
        return self.get_energy()


def test1(v=False):
    j = Jupiter("12t1.in", v=v)
    # outpt = j.show()
    # while j.steps < 10:
    #     j.do_time_step()
    #     outpt += j.show()
    # outpt = outpt[:-2]
    # with open("12s1.in") as fle:
    #     ans = fle.read()
    # return outpt == ans
    return j.run_sim(10) == 179


def test2():
    return Jupiter("12t2.in").run_sim(100) == 1940


@test
def tests():
    print(test1())
    print(test2())


if __name__ == "__main__":
    tests()
    print(Jupiter("12.in").run_sim(1000))
