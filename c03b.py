#!/usr/bin/env python3

from c03a import shortest_distance_to_crossing, data, t1, t2, t3


def combined_steps(p, paths):
    return sum([paths[i].index(p) for i in range(2)])


def fewest_combined_steps_to_crossing(s, v=False):
    return shortest_distance_to_crossing(s, combined_steps, v)


def tests(v=False):
    for s, a in zip((t1, t2, t3), (30, 610, 410)):
        print(fewest_combined_steps_to_crossing(s, v) == a)


if __name__ == "__main__":
    # print(fewest_combined_steps_to_crossing(t1, True))
    tests()
    print(fewest_combined_steps_to_crossing(data))
