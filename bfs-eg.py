#!/usr/bin/env python3

# ref https://www.geeksforgeeks.org/breadth-first-search-or-bfs-for-a-graph/

from lib import timing

from collections import defaultdict, deque


class Graph:
    def __init__(self):
        self.graph = defaultdict(list)

    def add_edge(self, u, v):
        self.graph[u].append(v)

    # uncommented code MUCH faster than commented code!
    # deque faster than list (350 vs 410ms for n=1M)
    @timing
    def bfs(self, strt):
        visited = [False] * (len(self.graph))
        visited[strt] = True
        # visited = [strt]

        queue = deque([strt])

        traversal = []

        while queue:
            parent = queue.popleft()
            traversal.append(parent)

            for child in self.graph[parent]:
                if not visited[child]:
                    # if child not in visited:
                    queue.append(child)
                    visited[child] = True
                    # visited.append(child)

        return traversal


if __name__ == "__main__":
    n = 1
    g = Graph()
    for i in range(n):
        g.add_edge(i, 1 + i)
        g.add_edge(1 + i, i)
        g.add_edge(i, 2 + i)
        g.add_edge(1 + i, 2 + i)
        g.add_edge(2 + i, i)
        g.add_edge(2 + i, 3 + i)
        g.add_edge(3 + i, 3 + i)

    strt = 2
    if n <= 20:
        print(f"BFS from {strt}: " + " ".join([str(i) for i in g.bfs(strt)]))
    else:
        g.bfs(strt)
