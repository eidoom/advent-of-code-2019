#!/usr/bin/env python3

from lib import Template
from c06a import process_map


class Search(Template):
    def __init__(self, filename, v=False):
        super().__init__(v)
        self.data = process_map(filename)
        self.piv(self.data)

    # find the object that obj is in orbit about
    def get_left(self, obj):
        for left, right in self.data:
            if right == obj:
                return left

    def get_path_to_centre(self, start, path=[]):
        if start == "COM":
            self.piv(path)
            return path
        left = self.get_left(start)
        return self.get_path_to_centre(left, path + [left])

    def get_intersection(self, path1, path2):
        for a in path1:
            if a in path2:
                p = a
                break
        return p

    def min_path(self, start, end):
        path1 = self.get_path_to_centre(start)
        path2 = self.get_path_to_centre(end)
        x = self.get_intersection(path1, path2)
        count = 0
        for p in path1:
            if p == x:
                break
            else:
                count += 1
        for p in path2:
            if p == x:
                break
            else:
                count += 1
        return count


def run(filename):
    test = Search(filename)
    return test.min_path("YOU", "SAN")


if __name__ == "__main__":
    print(run("06tb.in") == 4)
    print(run("06.in"))
