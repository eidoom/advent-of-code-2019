#!/usr/bin/env python3

from lib import get_data

img_data_test = "123456789012"
img_data = get_data("08.in")[0]


def split_layers(data, w, t):
    ll = w * t
    return [data[a: a + ll] for a in range(0, len(data), ll)]


def test():
    return split_layers(img_data_test, 3, 2)


def real(v=False):
    zeroes = 150
    for layer in split_layers(img_data, 25, 6):
        zeroes_here = sum([a == "0" for a in layer])
        if zeroes_here < zeroes:
            zeroes = zeroes_here
            least_layer = layer
            if v:
                print(f"Current least zeroes={zeroes}")
    ones = sum([a == "1" for a in least_layer])
    twos = sum([a == "2" for a in least_layer])
    if v:
        print(least_layer)
        print(f"length={len(least_layer)}")
        print(ones)
        print(twos)
    return ones * twos


if __name__ == "__main__":
    print(real())
