#!/usr/bin/env python3

from lib import Intcode, get_int_data, test


@test
def tests():
    t1d = get_int_data("09t1.in")
    t1 = Intcode(t1d)
    print(t1.compute() == t1d)
    t2 = Intcode("09t2.in")
    print(len(str(t2.compute()[-1])) == 16)
    t3d = get_int_data("09t3.in")
    t3 = Intcode(t3d)
    print(t3.compute()[-1] == t3d[1])


def boost():
    icp = Intcode("09.in")
    out = icp.compute([1])
    print(not out[:-2])
    print(out[-1])


if __name__ == "__main__":
    tests()
    boost()
