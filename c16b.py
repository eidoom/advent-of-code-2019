#!/usr/bin/env python3

from lib import test

from c16a import FFT


class FFT2(FFT):
    def __init__(self, inpt, v=False):
        pre_in = []
        for i in range(10000):
            pre_in.extend([int(i) for i in str(inpt)])
        ofst = int("".join([str(i) for i in pre_in[:7]]))
        super().__init__(pre_in, ofst, v=v)
        self.piv(self.inpt)
        self.piv(self.ofst)


@test
def tests():
    print(FFT2("03036732577212944063491565474664").fft() == "84462026")
    print(FFT2("02935109699940807407585447034323").fft() == "78725270")
    print(FFT2("03081770884921959731165446850517").fft() == "53553731")


if __name__ == "__main__":
    tests()
    print(FFT2("16.in").fft())
