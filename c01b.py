#! /usr/bin/env python3

from c01a import calc, data


def calc_fuel(mass):
    fuel = 0
    new = calc(mass)
    while new > 0:
        fuel += new
        new = calc(new)
    return fuel


if __name__ == "__main__":
    print(sum([calc_fuel(d) for d in data]))
