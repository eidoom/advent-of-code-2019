#!/usr/bin/env python3

from lib import Intcode


def draw(raw):
    jj = raw[::3]
    ii = raw[1::3]
    data = raw[2::3]
    width = max(jj)
    height = max(ii)
    screen = [[0 for _ in range(width)] for _ in range(height)]
    for i, j, d in zip(ii, jj, data):
        screen[i - 1][j - 1] = d
    return "".join(["".join([str(e) for e in row]) + "\n" for row in screen])


if __name__ == "__main__":
    print(draw([1, 2, 3, 6, 5, 4]))

    ic = Intcode("13.in")
    ot = ic.compute()
    dw = draw(ot)
    print(dw)
    print(sum([1 for e in dw if e == "2"]))
