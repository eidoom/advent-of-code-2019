#!/usr/bin/env python3

from collections import Counter

from lib import test
from c14a import Stoichiometry

# ============================ old method - fails =============================


class S2(Stoichiometry):
    def __init__(self, filename, v=False):
        super().__init__(filename, v)
        self.ore = self.get_ore()

    def get_ore_again(self):
        # Reset total used
        self.ttl = Counter()
        return self.get_ore()

    # exploit cycles to find answer (cf. N-body problem: day 12, 2019)
    def calc(self):
        # print(self.ltvrs)
        tot_ore = 1000000000000
        # for i in range(10):
        #     print(self.get_ore_again())
        #     print(self.ltvrs)
        # return None
        strt = self.ore
        # print(strt)
        cur = 0
        cycle_ore = 0
        # cycle period
        count = 0
        while cur != strt:
            # Note that leftovers are carried on from previous calculation
            cur = self.get_ore_again()
            cycle_ore += cur
            count += 1
        # print(cycle_ore)
        cycles = tot_ore // cycle_ore
        # print(count)
        # just from full cycles for now
        tot_fuel = cycles * count
        # print(tot_fuel)
        extra = tot_ore % cycle_ore
        # print(extra)
        while extra > 0:
            cur = self.get_ore_again()
            extra -= cur
            tot_fuel += 1
        return tot_fuel - 1


# =============================================================================


def binary_search(filename, base=2):
    # search for region of answer, within (base^n, base^(n+1))
    tot_ore = 1000000000000
    ore = 0
    fuel = 1
    while ore < tot_ore:
        fuel *= base
        ore = Stoichiometry(filename).get_ore(num=fuel)

    # perform binary search on region
    left = fuel // base
    right = fuel
    while left <= right:
        mid = (left + right) // 2
        ore = Stoichiometry(filename).get_ore(num=mid)
        if ore < tot_ore:
            left = mid + 1
        elif ore > tot_ore:
            right = mid - 1
    return mid - 1 if ore > tot_ore else mid


@test
def tests():
    ans = [82892753, 5586022, 460664]
    for i, a in enumerate(ans, start=3):
        sol = binary_search(f"14at{i}.in")
        print(True if sol == a else f"{sol}!={a}")


if __name__ == "__main__":
    tests()
    print(binary_search("14.in"))
