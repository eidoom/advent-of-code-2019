#! /usr/bin/env python3


from lib import get_int_data


def calc(num):
    return num // 3 - 2


data = get_int_data("01.in")

if __name__ == "__main__":
    print(sum([calc(d) for d in data]))
