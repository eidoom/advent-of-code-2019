#!/usr/bin/env python3

rin = "152085-670283"
pin = [int(a) for a in rin.split("-")]


# Optimise by generating >= numbers
def solve(inpt):
    l, u = inpt
    n = 0
    for a in range(l, u + 1):
        astr = str(a)
        if all([astr[i + 1] >= astr[i] for i in range(5)]) and any(
            [astr[i + 1] == astr[i] for i in range(5)]
        ):
            n += 1
    return n


if __name__ == "__main__":
    print(solve(pin))
