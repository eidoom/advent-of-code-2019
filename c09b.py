#!/usr/bin/env python3

from lib import Intcode, get_int_data


def get_distress_signal_coords():
    icp = Intcode("09.in")
    return icp.compute([2])[-1]


if __name__ == "__main__":
    print(get_distress_signal_coords())
