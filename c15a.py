#!/usr/bin/env python3

from lib import Intcode


class RepairDroid(Intcode):
    def __init__(self, v=False):
        super().__init__("15.in", feedback_mode=True, v=v)
        self.pos = (0, 0)
        self.wall = "#"
        self.unknown = " "
        self.count = 0
        self.map = {self.pos: str(self.count)}

    @staticmethod
    def compass(mvmt_cmd):
        return {1: (-1, 0), 2: (1, 0), 3: (0, 1), 4: (0, -1)}[mvmt_cmd]

    def get_move(self, mvmt_cmd):
        return tuple(a + b for a, b in zip(self.pos, self.compass(mvmt_cmd)))

    def send_move(self, mvmt_cmd):
        return self.compute([mvmt_cmd])

    def walk(self):
        while True:
            free = True
            while free:
                if self.v:
                    print(self.rndr_mp())
                    input()
                for mvmt_cmd in range(1, 5):
                    pot_mv = self.get_move(mvmt_cmd)
                    if pot_mv not in self.map.keys():
                        status = self.send_move(mvmt_cmd)
                        if status == 0:
                            self.map[pot_mv] = self.wall
                            if mvmt_cmd == 4:
                                free = False
                        else:
                            self.count += 1
                            self.map[pot_mv] = str(self.count)
                            self.pos = pot_mv
                            if status == 1:
                                break
                            elif status == 2:
                                return self.count
                    else:
                        if mvmt_cmd == 4:
                            free = False
            while not free:
                try:
                    best = float("inf")
                    for mvmt_cmd in range(1, 5):
                        pot_mv = self.get_move(mvmt_cmd)
                        val = self.map[pot_mv]
                        if val != self.wall and int(val) < best:
                            best = int(val)
                            nxt = mvmt_cmd
                    self.send_move(nxt)
                    self.pos = self.get_move(nxt)
                    self.count -= 1
                except KeyError:
                    free = True

    def rndr_mp(self):
        coords = [k for k, v in self.map.items()]
        ii, jj = zip(*coords)
        mp = ""
        for i in range(min(ii), max(ii) + 1):
            for j in range(min(jj), max(jj) + 1):
                try:
                    sym = self.map[(i, j)]
                    try:
                        sym = str(int(sym) % 10)
                    except ValueError:
                        pass
                except KeyError:
                    sym = self.unknown
                mp += sym
            mp += "\n"
        return mp


if __name__ == "__main__":
    rd = RepairDroid()
    print(rd.walk())
