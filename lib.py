#!/usr/bin/env python3

from functools import wraps
from time import perf_counter


def get_lines(filename):
    with open(filename, "r") as file:
        return [line.rstrip("\n") for line in file.readlines()]


def get_data(filename):
    data = [line.split(",") for line in get_lines(filename)]
    if len(data) == 1:
        return data[0]
    else:
        return [line[0] for line in data] if len(data[0]) == 1 else data


def get_int_data(file):
    return [
        [int(a) for a in line] if type(line) == list else int(line)
        for line in get_data(file)
    ]


class Template:
    def __init__(self, v_):
        self.v = v_

    # print if verbose
    def piv(self, *args, **kwargs):
        if self.v:
            print(*args, **kwargs)


class Intcode(Template):
    def __init__(
        self, program, feedback_mode=False, return_memory=False, v=False,
    ):
        super().__init__(v)
        if type(program) == list:
            self.memory = program.copy()  # memory state
        elif type(program) == str:
            self.memory = get_int_data(program)
        self.i = 0  # index
        self.feedback_mode = feedback_mode
        self.relative_base = 0
        self.return_memory = return_memory
        self.modes = ["position"] * 3

    def get_address(self, rel):
        index = self.i + rel
        mode = self.modes[rel - 1]
        if mode == "position":
            address = self.memory[index]
        elif mode == "immediate":
            address = index
        elif mode == "relative":
            address = self.relative_base + self.memory[index]
        else:
            raise ValueError(f"Opcode mode {mode} unrecognised")
        if address < 0:
            raise IndexError(f"Address {address} is not a positive integer!")
        self.piv(f"address={address}")
        return address

    # arguments:
    # relative index (from self.i), either 1, 2, or 3
    # opcode mode (position, immediate, or relative)
    def ref(self, rel):
        address = self.get_address(rel)
        try:
            value = self.memory[address]
        except IndexError:
            # self.memory += [0] * (address - len(self.memory) + 1)
            value = 0
        self.piv(f"retrieved value {value}")
        return value

    def alloc(self, rel, value):
        address = self.get_address(rel)
        try:
            self.memory[address] = value
        except IndexError:
            self.memory += [*[0] * (address - len(self.memory)), value]

    def compute(self, inpts=None):
        self.piv(f"memory={self.memory}")
        outpt = None if self.feedback_mode else []
        count = 0
        while True:
            # immediate mode off by default
            # (ie. position mode on) for all parameters
            opcodeStr = str(self.memory[self.i])
            self.piv(f"i={self.i}")
            self.piv("instruction=" + opcodeStr)
            for a, b in zip(range(3, 6), range(3)):
                try:
                    if opcodeStr[-a] == "0":
                        self.piv(f"Parameter {b+1} in position mode")
                        self.modes[b] = "position"
                    elif opcodeStr[-a] == "1":
                        self.piv(f"Parameter {b+1} in immediate mode")
                        self.modes[b] = "immediate"
                    elif opcodeStr[-a] == "2":
                        self.piv(f"Parameter {b+1} in relative mode")
                        self.piv(f"Relative base={self.relative_base}")
                        self.modes[b] = "relative"
                    else:
                        raise ValueError(
                            f"Opcode mode {opcodeStr[-a]} for parameter {b+1} unrecognised"
                        )
                except IndexError:
                    self.modes[b] = "position"
            opcode = int(opcodeStr[-2:])
            self.piv(f"opcode={opcode}")
            # addition
            if opcode == 1:
                self.piv("addition mode")
                value = self.ref(1) + self.ref(2)
                self.alloc(3, value)
                self.i += 4
            # multiplication
            elif opcode == 2:
                self.piv("multiplication mode")
                value = self.ref(1) * self.ref(2)
                self.alloc(3, value)
                self.i += 4
            # input
            elif opcode == 3:
                self.piv("input mode")
                try:
                    value = inpts[count]
                except TypeError:
                    raise TypeError("Intcode program input should be a list!")
                except IndexError:
                    raise IndexError(
                        "Intcode program calls for more inputs than are provided!"
                    )
                self.piv(f"input={value}")
                self.alloc(1, value)
                count += 1
                self.i += 2
            # output
            elif opcode == 4:
                self.piv("output mode")
                value = self.ref(1)
                self.piv(f"output={value}")
                self.i += 2
                if self.feedback_mode:
                    return value
                outpt += [value]
            # jump-if-true
            elif opcode == 5:
                self.piv("jump-if-true mode")
                if self.ref(1):
                    self.i = self.ref(2)
                else:
                    self.i += 3
            # jump-if-false
            elif opcode == 6:
                self.piv("jump-if-false mode")
                if not self.ref(1):
                    self.i = self.ref(2)
                else:
                    self.i += 3
            # less than
            elif opcode == 7:
                self.piv("less than mode")
                value = int(self.ref(1) < self.ref(2))
                self.alloc(3, value)
                self.i += 4
            # equals
            elif opcode == 8:
                self.piv("equals mode")
                value = int(self.ref(1) == self.ref(2))
                self.alloc(3, value)
                self.i += 4
            # relative base offset
            elif opcode == 9:
                self.piv("relative base offset mode")
                param = self.ref(1)
                self.relative_base += param
                self.i += 2
                self.piv(f"relative base={self.relative_base}")
            # halt
            elif opcode == 99:
                self.piv("halt mode")
                return self.memory if self.return_memory else outpt
            else:
                raise ValueError(f"Opcode {opcode} not recognised at address {self.i}")
            self.piv(f"memory={self.memory}")


def test(func):
    @wraps(func)
    def func_wrapper(*args, **kwargs):
        print("======= tests =======")
        solution = func(*args, **kwargs)
        print("=====================")
        return solution

    return func_wrapper


def time_formatter(start, rounding=1):
    """
    Stolen from my Project Euler solutions.
    Generates a string telling the time passed since start, with automatic unit scaling.
    :param start: The initial time.
    :param rounding: The number of decimal points to include for the returned time.
    :return: The elapsed time since start, with automatic units.
    """
    time = perf_counter() - start
    power = 0
    unit = ""
    for index, prefix in zip(range(-3, 10, 3), ("kilo", "", "milli", "micro", "nano")):
        if time * (10 ** index) < 10 ** 3:
            power = index
            unit = prefix
    return f"{round(10 ** power * time, rounding)} {unit}seconds"


def timing(function):
    """
    Stolen from my Project Euler solutions.
    Decorator that returns function and prints how long it took to evaluate.
    :param function: A function, e.g. function(), that returns some solution.
    :return: The function solution (and print the time it took to run).
    """

    @wraps(function)
    def wrapped_function(*args, **kwargs):
        start = perf_counter()
        solution = function(*args, **kwargs)
        print(f"Function {function.__name__} took {time_formatter(start)}.")
        return solution

    return wrapped_function


if __name__ == "__main__":
    exit()
