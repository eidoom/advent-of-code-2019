#!/usr/bin/env python3

from lib import get_data

data = get_data("03.in")
t1 = get_data("03t1.in")
t2 = get_data("03t2.in")
t3 = get_data("03t3.in")


def rectilinear_distance(p, paths):
    return abs(p[0]) + abs(p[1])


def shortest_distance_to_crossing(instruction_set, distance_function, v=False):
    paths = [[(0, 0)], [(0, 0)]]
    for path, instructions in zip(paths, instruction_set):
        k = 0
        for i in instructions:
            for j in range(int(i[1:])):
                ii, jj = path[k]
                if i[0] == "R":
                    path += [(ii, jj + 1)]
                elif i[0] == "L":
                    path += [(ii, jj - 1)]
                elif i[0] == "U":
                    path += [(ii - 1, jj)]
                elif i[0] == "D":
                    path += [(ii + 1, jj)]
                else:
                    raise ValueError(f"Direction {i[0]} unrecognised")
                k += 1
        if v:
            print(instructions)
            print(path)
    intersection = set(paths[0]) & set(paths[1])
    intersection -= {(0, 0)}
    if v:
        print(intersection)
    ans = 10**4
    for i in intersection:
        d = distance_function(i, paths)
        if v:
            print(d)
        if d < ans:
            ans = d
    return ans


def tests(v=False):
    for s, a in zip((t1, t2, t3), (6, 159, 135)):
        print(shortest_distance_to_crossing(s, rectilinear_distance, v) == a)


if __name__ == "__main__":
    # print(shortest_distance_to_crossing(t1, rectilinear_distance, True))
    tests()
    print(shortest_distance_to_crossing(data, rectilinear_distance))
