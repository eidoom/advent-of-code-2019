#!/usr/bin/env python3

from lib import Intcode, test, Template


class Test(Template):
    def __init__(self):
        self.n = 0

    def single_test(self, icp, inpt, ans):
        ic = Intcode(icp)
        sol = ic.compute([inpt])[-1] == ans
        ns = "0" + str(self.n) if self.n < 10 else str(self.n)
        self.n += 1
        print(f"{ns} {sol}")

    @test
    def all(self):
        t1 = [3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8]
        t2 = [3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8]
        t3 = [3, 3, 1108, -1, 8, 3, 4, 3, 99]
        t4 = [3, 3, 1107, -1, 8, 3, 4, 3, 99]
        t5 = [3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9]
        t6 = [3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1]
        t7 = "05t7.in"

        icps = [
            a
            for row in [
                [a] * m for a, m in zip((t1, t2, t3, t4, t5, t6, t7), (*[2] * 6, 3))
            ]
            for a in row
        ]
        inpts = (8, 7, 7, 9, 8, 7, 7, 9, 1, 0, 1, 0, 7, 8, 9)
        answers = [1, 0] * 6 + [999 + i for i in range(3)]
        for r in zip(icps, inpts, answers):
            self.single_test(*r)


if __name__ == "__main__":
    t = Test()
    t.all()

    # output zero indicates subroutine successful
    # final output is diagnostic code
    print("Diagnostic code:")
    intcode = Intcode("05.in")
    print(intcode.compute([5])[-1])  # gives correct answer
