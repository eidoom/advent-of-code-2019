#!/usr/bin/env python3

from lib import Intcode, test


@test
def tests():
    t1 = [3, 0, 4, 0, 99]
    ict1 = Intcode(t1)
    print(ict1.compute([1])[-1] == 1)
    t2 = [1002, 4, 3, 4, 33]
    ict2 = Intcode(t2)
    print(not ict2.compute([1]))


if __name__ == "__main__":
    tests()

    intcode = Intcode("05.in")
    outpt = intcode.compute([1])
    # output zero indicates subroutine successful
    print(outpt[:-2])
    # final output is diagnostic code
    print(outpt[-1])
