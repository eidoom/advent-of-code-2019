#!/usr/bin/env python3.8

from c10a import MonStaPos


class GiantLaser(MonStaPos):
    def __init__(self, filename, v=False):
        super().__init__(filename, v)
        self.vapour_trail = []
        self.drns = []
        self.debris = "*"
        self.monitoring_station = "X"

    def id_dir(self, i, j):
        if not i:
            return 0
        elif not j:
            return i * float("inf")
        else:
            return i / j

    def sort_dirs(self, drns):
        return [
            k
            for k, v in sorted(
                {a: self.id_dir(*a) for a in drns}.items(), key=lambda a: a[1]
            )
        ]

    # make concise with sets?
    # dir: start UP and rotate clockwise
    def get_dirs(self):
        if self.drns:
            return self.drns
        drns_r = []
        drns_l = []
        for i in range(-self.height + 1, self.height):
            for j in range(-self.width + 1, self.width):
                if i or j:
                    new_dir = self.normalise_dir(i, j)
                    if new_dir[1] >= 0:
                        if new_dir not in drns_r:
                            drns_r += [new_dir]
                    else:
                        if new_dir not in drns_l:
                            drns_l += [new_dir]
        drn_r_ids_ord = self.sort_dirs(drns_r)
        drn_l_ids_ord = self.sort_dirs(drns_l)
        self.drns = drn_r_ids_ord + drn_l_ids_ord
        return self.drns

    def set_map_pt(self, i, j, value):
        self.mp[i][j] = value

    # must set monitoring station position before firing
    def fire_laser(self, i=1, n=None):
        for drn in self.get_dirs():
            if loc := self.see_asteroid(drn):
                self.set_map_pt(*loc, str(i % 10) if self.v else self.debris)
                self.vapour_trail += [loc]
                if i == n:
                    return loc
                i += 1
        if self.asteroid in [a for row in self.mp for a in row]:
            self.fire_laser(i, n)

    def draw_map(self):
        return "\n".join(["".join(line) for line in self.mp])


def test1():
    t1 = GiantLaser("10t6.in")
    t1.set_pos(*t1.find_best()[1])
    t1.set_map_pt(*t1.pos, t1.monitoring_station)
    t1.v = True
    t1.fire_laser(n=9)
    s1 = GiantLaser("10s6.in")
    print(t1.draw_map() == s1.draw_map())


def test2():
    t2 = GiantLaser("10t5.in")
    t2.set_pos(13, 11)
    t2.set_map_pt(*t2.pos, t2.monitoring_station)
    t2.fire_laser()
    for i, pt in zip(
        (10, 20, 50, 100, 199, 200, 201, 299),
            ((8, 12), (0, 16), (9, 16), (16, 10), (6, 9), (2, 8), (9, 10), (1, 11))
    ):
        print(t2.vapour_trail[i-1] == pt)


def complete_vapourisation():
    gl = GiantLaser("10.in")
    gl.set_pos(17, 13)
    return gl.fire_laser(n=200)


if __name__ == "__main__":
    test1()
    test2()

    y, x = complete_vapourisation()
    print(100*x+y)
