#!/usr/bin/env python3

from copy import deepcopy

from lib import Intcode


class Game(Intcode):
    def __init__(self, prgm):
        super().__init__(prgm, feedback_mode=True)
        # set free play mode
        self.memory[0] = 2
        # end bool for AI
        self.end = False
        self.score = 0
        # raw output from intcode program
        ot = []
        # initialise by running until needs input
        self.dumb_loop(self.init_posns, ot)
        # split up ot into i, j, id data
        jj = ot[::3]
        ii = ot[1::3]
        raw = ot[2::3]
        # find screen dimensions
        self.width = max(jj)
        self.height = max(ii)
        # init screen
        self.scrn = [[0 for _ in range(self.width)] for _ in range(self.height)]
        # populate screen using ot
        for i, j, d in zip(ii, jj, raw):
            if i < 0 or j < 0:
                raise ValueError
            self.upd_posn(i, j, d, self.scrn)

    # method to build ot
    @staticmethod
    def init_posns(i, j, d, lst):
        lst += [j, i, d]

    # method to update screen
    @staticmethod
    def upd_posn(i, j, d, lst):
        lst[i - 1][j - 1] = d

    def dumb_loop(self, func, lst):
        while not self.end:
            try:
                self.ic2gm(func, lst)
            except IndexError:
                break

    # run intcode machine and translate its output to our screen
    def ic2gm(self, func, lst, inpt=None):
        inpt = [] if inpt is None else [inpt]
        j, i, d = [self.compute(inpt) for _ in range(3)]
        if j is None:
            self.end = True
        else:
            if j == -1:
                if i != 0:
                    raise ValueError
                self.score = d
            else:
                func(i, j, d, lst)

    # manual play
    def play(self):
        self.draw()
        while self.not_end() and not self.end:
            inpt = None
            while inpt is None:
                try:
                    inpt = {"a": -1, "d": 1, "": 0}[input()]
                except KeyError:
                    pass
            self.ic2gm(self.upd_posn, self.scrn, inpt)
            self.dumb_loop(self.upd_posn, self.scrn)
            self.draw()
        print("Game over")

    # print screen
    def draw(self, brd=None):
        if brd is None:
            brd = self.scrn
        tmp = [
            [{0: " ", 1: "|", 2: "#", 3: "-", 4: "*", 5: "."}[d] for d in row]
            for row in brd
        ]
        print(
            "  |"
            + "|".join([str(n % 10) for n in range(self.width)])
            + "|\n"
            + "".join(
                [
                    (" " if i < 10 else "")
                    + str(i)
                    + "|"
                    + "|".join(row)
                    + "|"
                    + (str(self.score) if i == self.height - 1 else "\n")
                    for i, row in enumerate(tmp)
                ]
            )
        )

    # bool: game is not ended, for  manual play
    def not_end(self):
        return bool(sum([1 for e in [a for row in self.scrn for a in row] if e == 2]))

    # get (first) index of obj with id idn
    def find_obj(self, idn):
        bii, br = [(i, x) for i, x in enumerate(game.scrn) if idn in x][0]
        return bii, br.index(idn)

    def find_ball(self):
        return self.find_obj(4)

    def find_paddle(self):
        return self.find_obj(3)[1]

    # calculate where ball will end up on paddle's horizontal line
    def path(self, pos, ori, print_path=False, incremental=False):
        tmp = deepcopy(self.scrn)
        i, j = pos
        a, b = ori
        while i != self.height - 2:
            i, j = [c + d for c, d in zip((i, j), (a, b))]  # next pos
            if j == 0 or j == self.width - 2:  # hit left or right wall
                b *= -1
            elif i == 0:  # hit top
                a *= -1
            elif tmp[i + a][j] == 3:  # hit paddle vertically
                break
            elif tmp[i + a][j + b] == 3:  # hit paddle diagonally
                break
            elif tmp[i][j + b] == 2:  # hit block vertically
                tmp[i][j + b] = 0
                b *= -1
            elif tmp[i + a][j] == 2:  # hit block horizontally
                tmp[i + a][j] = 0
                a *= -1
            elif tmp[i + a][j + b] == 2:  # bounce off block on diagonal
                tmp[i + a][j + b] = 0
                a *= -1
                b *= -1
            tmp[i][j] = 5
            if incremental:
                input()
                self.draw(tmp)
        if print_path and not incremental:
            self.draw(tmp)
        return j

    # AI to play the game automatically
    def ai(
        self,
        print_turn=False,
        print_path=False,
        increment_turn=False,
        increment_path=False,
    ):
        if print_turn:
            self.draw()
        init_pos = self.find_ball()
        init_ori = (1, 1)  # start SE
        pos = tuple(p - o for p, o in zip(init_pos, init_ori))
        while not self.end:
            new_pos = self.find_ball()
            ori = tuple(n - o for n, o in zip(new_pos, pos))
            aim = self.path(pos, ori, print_path=print_path, incremental=increment_path)
            cur = self.find_paddle()
            if aim < cur:
                inpt = -1
            elif aim > cur:
                inpt = 1
            else:
                inpt = 0
            self.ic2gm(self.upd_posn, self.scrn, inpt)
            self.dumb_loop(self.upd_posn, self.scrn)
            if increment_turn:
                input()
            if print_turn:
                self.draw()
            pos = new_pos
        return self.score


if __name__ == "__main__":
    game = Game("13.in")

    game.ai(print_turn=True)

    # print(game.ai())

    # game.play()
