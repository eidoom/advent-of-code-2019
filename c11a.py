#!/usr/bin/env python3

from lib import Template, Intcode


class f:
    RED = "\033[91m"
    END = "\033[0m"


class Painter(Template):
    def __init__(self, w=5, h=5, debug=False, v=False):
        super().__init__(v)
        self.ic = Intcode("11.in", feedback_mode=True)
        self.black = "."
        self.white = "#"
        self.left = "<"  # 270
        self.right = ">"  # 90
        self.up = "^"  # 0
        self.down = "v"  # 180
        self.width = w
        self.height = h
        self.hull = self.build_map(self.black)  # starts all black
        self.trail = self.build_map("o")
        # drn in clockwise degrees, zero is UP
        self.drn = 0
        # start in centre, coords: i, j
        self.pos = (self.height // 2, self.width // 2)
        self.set_plate(self.trail, f.RED + self.deg2str_drn() + f.END)
        self.int2str_col_map = {0: self.black, 1: self.white}
        self.str2int_col_map = self.rev_map(self.int2str_col_map)
        self.lims = (self.width - 1, self.height - 1)
        self.painted = set()
        self.debug = debug

        if self.debug:
            self.piv(self.draw_hull())
            self.piv(self.draw(self.trail))

    def rev_map(self, mp):
        return {v: k for k, v in mp.items()}

    def build_map(self, plate):
        return [[plate] * self.width for _ in range(self.height)]

    def get_plate(self, mp):
        i, j = self.pos
        return mp[i][j]

    # return in int format
    def get_plate_col(self):
        return self.str2int_col(self.get_plate(self.hull))

    def paint(self):
        while True:
            old_col = self.get_plate_col()
            new_col = self.ic.compute([old_col])
            if new_col is None:
                self.piv("\n" + self.draw_hull() + "\n" + self.draw(self.trail) + "\n")
                return None
            self.set_plate_col(new_col)
            self.painted.add(self.pos)
            trn = self.ic.compute()
            self.set_drn(trn)
            if self.v:
                i, j = self.pos
                self.set_plate(
                    self.trail, self.get_plate(self.trail)[len(f.RED) : -len(f.END)]
                )
            self.pos = tuple(
                min(max(0, a + b), lim)
                for a, b, lim in zip(self.pos, self.calc_move(), self.lims)
            )
            self.set_plate(self.trail, f.RED + self.deg2str_drn() + f.END)
            if self.debug:
                print(self.draw_hull())
                print(self.draw(self.trail))
                input()

    def deg2str_drn(self):
        return {0: self.up, 90: self.right, 180: self.down, 270: self.left}[self.drn]

    # input: turn int
    def set_drn(self, trn):
        self.drn = (self.drn + self.int2deg_trn(trn)) % 360

    def calc_move(self):
        return {0: (-1, 0), 90: (0, 1), 180: (1, 0), 270: (0, -1)}[self.drn]

    @staticmethod
    def draw(mp):
        return "\n".join(["".join(line) for line in mp])

    def draw_hull(self):
        return self.draw(self.hull)

    def set_plate(self, mp, value):
        i, j = self.pos
        mp[i][j] = value

    # input: colour int
    def set_plate_col(self, col):
        self.set_plate(self.hull, self.int2str_col(col))

    # colour int -> str
    def int2str_col(self, col):
        return self.int2str_col_map[col]

    def str2int_col(self, col):
        return self.str2int_col_map[col]

    # turn int -> degrees
    @staticmethod
    def int2deg_trn(trn):
        return {0: -90, 1: 90}[trn]


if __name__ == "__main__":
    s = 137
    p = Painter(w=s, h=s)
    p.paint()
    print(len(p.painted))

