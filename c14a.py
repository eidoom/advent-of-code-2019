#!/usr/bin/env python3

from lib import get_lines, test, Template

from math import ceil

# https://docs.python.org/3.8/library/collections.html#collections.Counter
from collections import Counter


class Stoichiometry(Template):
    def __init__(self, filename, v=False):
        super().__init__(v)
        # get reactant rules from file
        self.rctns = self.get_rctns(filename)
        # total used
        self.ttl = Counter()
        # leftovers
        self.ltvrs = Counter()

    @staticmethod
    def get_rctns(filename):
        return [
            [
                {
                    v: int(k)
                    for k, v in [pair.strip().split(" ") for pair in lst.split(",")]
                }
                for lst in line.split(" => ")
            ]
            for line in get_lines(filename)
        ]

    def vrbs_rgnts(self, rgnts):
        return " & ".join([f"{q} {r}" for r, q in rgnts.items()])

    def vrbs_ltvr(self, chem, ltvr):
        return f", with {ltvr} {chem} leftover" if ltvr else ""

    def vrbs_rsd(self, chem, rsd):
        return f", using {rsd} {chem} from leftovers" if rsd else ""

    # return reagents cost, leftover product
    def get_rgnts(self, chem="FUEL", num=1):
        for rgnts, prdt in self.rctns:
            if chem in prdt:
                # how much new chem is required
                new = max(0, num - self.ltvrs[chem])
                # how much existing chem was used (reused)
                rsd = num - new
                # remove used leftovers from leftovers counter
                self.ltvrs[chem] -= rsd
                # how much product is made per reaction
                qty = prdt[chem]
                # how many times reaction is perfomed
                lts = ceil(new / qty)
                # unused product
                ltvr = lts * qty - new
                # total reagent requirements for chem
                ttl = Counter({rgnt: lts * amnt for rgnt, amnt in rgnts.items()})
                self.piv(
                    f"{num} {chem} needs "
                    + self.vrbs_rgnts(ttl)
                    + self.vrbs_ltvr(chem, ltvr)
                    + self.vrbs_rsd(chem, rsd)
                )
                return ttl, ltvr

    # populate ttl & ltvrs
    def count(self, chem="FUEL", num=1):
        self.ttl[chem] += num
        rgnts, ltvr = self.get_rgnts(chem, num)
        if "ORE" not in rgnts:
            self.ltvrs[chem] += ltvr
            for rgnt, qty in rgnts.items():
                self.count(rgnt, qty)

    # calculate ore cost
    def get_ore(self, chem="FUEL", num=1):
        # populate ttl & ltvrs
        self.count(chem, num)
        self.piv("Total used: " + self.vrbs_rgnts(self.ttl))
        self.piv("Surplus made: " + self.vrbs_rgnts(self.ltvrs))
        # Don't need to worry about KeyError as with dict
        # since Counter returns 0 for missing items
        return sum(
            [self.get_rgnts(chem, qty)[0]["ORE"] for chem, qty in self.ttl.items()]
        )


@test
def tests(v=False):
    test_answers = [31, 165, 13312, 180697, 2210736, 20]
    for i, ans in enumerate(test_answers, start=1):
        s = Stoichiometry(f"14at{i}.in", v=v)
        ore = s.get_ore()
        print(True if ore == ans else f"{ore}!={ans}")
        s.piv()


if __name__ == "__main__":
    tests()
    print(Stoichiometry("14.in").get_ore())
