#!/usr/bin/env python3

from c02a import ic, fix


def find(init_mem, v=False):
    for noun in range(0, 100):
        for verb in range(0, 100):
            command = 100 * noun + verb
            if v:
                print(f"command={command}")
            state = fix(noun, verb, init_mem)
            if v:
                print(f"state={state}")
            if state == 19690720:
                return command


if __name__ == "__main__":
    print(find(ic))
