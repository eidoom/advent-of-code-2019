#!/usr/bin/env python3

from c11a import Painter

if __name__ == "__main__":
    s = 99
    p = Painter(w=s, h=s)
    p.set_plate_col(1)
    p.paint()
    print(p.draw_hull())
