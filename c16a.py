#!/usr/bin/env python3

from lib import test, get_int_data, Template


class FFT(Template):
    def __init__(self, inpt, ofst=0, v=False):
        super().__init__(v_=v)
        if type(inpt) is str and inpt[-3:] == ".in":
            inpt = get_int_data(inpt)[0]
        self.inpt = inpt if type(inpt) is list else [int(i) for i in str(inpt)]
        self.bs_ptn = [0, 1, 0, -1]
        self.ofst = ofst

    def phase(self, inpt):
        otpt = []
        for i in range(1, len(inpt) + 1):
            ptrn = []
            for x in self.bs_ptn:
                ptrn.extend([x] * i)
            m = len(inpt) // len(ptrn) + 1
            ptrn = (ptrn * m)[1:]
            otpt.append(int(str(sum([inp * pat for inp, pat in zip(inpt, ptrn)]))[-1]))
        return otpt

    def fft(self, n=100):
        inpt = self.inpt
        for i in range(n):
            inpt = self.phase(inpt)
            self.piv(inpt)
        return "".join([str(i) for i in inpt[self.ofst : self.ofst + 8]])


@test
def tests():
    print(FFT("80871224585914546619083218645595").fft() == "24176176")
    print(FFT("19617804207202209144916044189917").fft() == "73745418")
    print(FFT("69317163492948606335995924319873").fft() == "52432133")


if __name__ == "__main__":
    tests()
    print(FFT("16.in").fft())
