#!/usr/bin/env python3

from c08a import split_layers, img_data

test = "0222112222120000"


def build_img(data, w, t):
    # print(split_layers(test, w, t))
    out = []
    # loop over all image pixels
    for i in range(w * t):
        # find visible pixel from layers
        for layer in split_layers(data, w, t):
            if layer[i] != "2":
                out += [layer[i]]
                break
    return split_layers(out, w, 1)


def print_img(data, w, t):
    pd = build_img(data, w, t)
    for line in pd:
        print("".join(line).replace("0", " ").replace("1", "0"))


if __name__ == "__main__":
    print("Test:")
    print_img(test, 2, 2)
    print("Image:")
    print_img(img_data, 25, 6)
