#!/usr/bin/env python3


def single(inpt):
    astr = str(inpt)
    if all([astr[i + 1] >= astr[i] for i in range(5)]):
        for i in range(5):
            if astr[i + 1] == astr[i]:
                double = True
                try:
                    if astr[i + 1] == astr[i + 2]:
                        double = False
                except IndexError:
                    pass
                try:
                    if astr[i] == astr[i - 1]:
                        double = False
                except IndexError:
                    pass
                if double:
                    return True
    return False


def solve(inpt):
    l, u = inpt
    n = 0
    for a in range(l, u + 1):
        if single(a):
            n += 1
    return n


if __name__ == "__main__":
    # Tests
    print(single(112233))
    print(not single(123444))
    print(single(111122))

    rin = "152085-670283"
    pin = [int(a) for a in rin.split("-")]
    print(solve(pin))
