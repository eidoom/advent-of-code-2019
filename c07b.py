#!/usr/bin/env python3

from c07a import maximise_output
from lib import Intcode, test

inpt = 0


# argument: Intcode program, phase setting sequence, input
def run_feedback_loop(icp, pss, inpt_, v=False):
    amps = [Intcode(icp, feedback_mode=True, v=v) for _ in range(5)]
    tmp = inpt_
    for amp, ps in zip(amps, pss):
        tmp = amp.compute([ps, tmp])
    if v:
        print("Initialisation complete")
    while True:
        for i, amp in enumerate(amps):
            new_tmp = amp.compute([tmp])
            if new_tmp is None:
                if i == 4:
                    return tmp
            else:
                tmp = new_tmp


@test
def tests():
    t1pss = [9, 8, 7, 6, 5]
    t1mts = 139629729
    print(run_feedback_loop("07bt1.in", t1pss, inpt) == t1mts)
    t2pss = [9, 7, 8, 5, 6]
    t2mts = 18216
    print(run_feedback_loop("07bt2.in", t2pss, inpt) == t2mts)


if __name__ == "__main__":
    tests()
    print(maximise_output("07.in", inpt, run_feedback_loop))
