#include <iostream>
#include <string>
#include <array>
#include <fstream>
#include <regex>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <algorithm>
#include <numeric>

// <iostream>
using std::cout;
using std::endl;
// <string>
using std::string;
using std::stoi;
using std::getline;
// <array>
using std::array;
// <fstream>
using std::fstream;
// <regex>
using std::regex;
using std::sregex_iterator;
using std::smatch;
// <cstdlib>
using std::size_t;
// <cmath>
using std::abs;
// <vector>
using std::vector;
// <algorithm>
using std::find;
// <numeric>
using std::accumulate;
using std::lcm;


class Jupiter {
private:
    static const size_t numMoons{4};
    static const size_t numAxes{3};
    static const size_t numDims{2 * numAxes};

    array <array<array < int, 2>, numMoons>, numAxes> kin;
    array<unsigned long, numAxes> steps{0, 0, 0};
    array <vector<array < array < int, 2>, numMoons>>, numAxes> his;

    void getInitKin(const string &fileName) {
        fstream data(fileName);
        string line;
        size_t m{0};
        while (getline(data, line)) {
            regex num_regex("(-?\\d+)");
            sregex_iterator nums_begin{sregex_iterator(line.begin(), line.end(), num_regex)};
            sregex_iterator nums_end{sregex_iterator()};
            size_t a{0};
            for (sregex_iterator i{nums_begin}; i != nums_end; ++i, ++a) {
                smatch match{*i};
                string match_str{match.str()};
                int match_int{stoi(match_str)};
                kin[a][m][0] = match_int;
                kin[a][m][1] = 0;
            }
            ++m;
        }
    }

    void printPt(array <array<int, 2>, numMoons> &pt) {
        for (size_t m{0}; m < numMoons; ++m) {
            for (size_t a{0}; a < 2; ++a) {
                cout << pt[m][a] << " ";
            }
            cout << endl;
        }
    }

    void printKin(size_t a) {
        cout << "After " << steps[a] << " steps in axis " << a << ": " << endl;
        printPt(kin[a]);
    }

    void printHis(size_t a) {
        for (array <array<int, 2>, numMoons> &pt: his[a]) {
            printPt(pt);
        }
    }

    int getEnergy() {
        int en{0};
        for (size_t m{0}; m < numMoons; ++m) {
            int potEn{0};
            int kinEn{0};
            for (size_t i{0}; i < numAxes; ++i) {
                potEn += abs(kin[i][m][0]);
                kinEn += abs(kin[i][m][1]);
            }
            en += potEn * kinEn;
        }
        return en;
    }

    void doAxisTimeStep(const size_t c) {
        for (size_t a{0}; a < numMoons; ++a) {
            for (size_t b{a + 1}; b < numMoons; ++b) {
                int first = kin[c][a][0];
                int second = kin[c][b][0];
                if (first > second) {
                    --kin[c][a][1];
                    ++kin[c][b][1];
                } else if (first < second) {
                    ++kin[c][a][1];
                    --kin[c][b][1];
                }
            }
            kin[c][a][0] += kin[c][a][1];
        }
        ++steps[c];
    }

public:
    int runSim(const unsigned long n) {
        while (steps[0] < n) {
            for (size_t c{0}; c < numAxes; ++c) {
                doAxisTimeStep(c);
            }
        }
        return getEnergy();
    }

// each axis is independent and cycles back to the repeated value in a regular cycle
// so we can find first repetition for each axis then find least common multiple of these for answer
// watch out for big integers!
    unsigned long findRepeat() {
        for (size_t c{0}; c < numAxes; ++c) {
            while (find(his[c].begin(), his[c].end(), kin[c]) == his[c].end()) {
                his[c].push_back(kin[c]);
                doAxisTimeStep(c);
            }
        }
        return accumulate(
                steps.begin(),
                steps.end(),
                static_cast<unsigned long>(1),
                [](unsigned long a, unsigned long b) { return lcm<unsigned long>(a, b); }
        );
    }

    Jupiter(const string filename) {
        getInitKin(filename);
    }
};

int main(int argc, char **argv) {
    Jupiter t1a("12t1.in");
    cout << (t1a.runSim(10) == 179) << endl;
    Jupiter t1b("12t1.in");
    cout << (t1b.findRepeat() == 2772) << endl;
    Jupiter t2a("12t2.in");
    cout << (t2a.runSim(100) == 1940) << endl;
    Jupiter t2b("12t2.in");
    cout << (t2b.findRepeat() == 4686774924) << endl;
    Jupiter ra("12.in");
    cout << (ra.runSim(1000) == 5517) << endl; // answer for (a)
    Jupiter rb("12.in");
    cout << rb.findRepeat() << endl;
    return 0;
}